package
{
	import starling.display.Sprite;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Alexander.kolobov
	 */
	public class StarlingRoot extends Sprite
	{
		private var gui:GUI;
		private var operand1:Number;
		private var operand2:Number;
		private var operand2repeat:Number;
		private var result:String;
		private var operation:String = "";
		private var calculate:Boolean = true; // не считать если нажата кнопка другой операции		
		private var clearDisplay:Boolean = false; // очистить дисплей		
		private var waitInput:Boolean = false;
		private var repeat:Boolean = false; // повторяем операциюпри повторном нажатии кнопки "="
		private var dontPrintResult:Boolean = false;
		private var fistPress:Boolean = true; // не выполнять операции пока не нажата какая ни будь цифра
		
		private function executeCalculation(operation:String):void
		{
			operand2 = operand2repeat;
			
			calculate = true;
			
			if (this.operation != operation && this.operation != "")
			{
				operand1 = Number(result);
				gui.calcTextField.text = result;
				calculate = false;
			}
			
			this.operation = operation;
			
			if (!waitInput)
			{
				
				waitInput = true;
				
				if (isNaN(operand1))
				{
					operand1 = Number(gui.calcTextField.text);
					clearDisplay = true;
				}
				else
				{
					if (calculate)
					{
						if (!repeat)
							operand2 = operand2repeat = Number(gui.calcTextField.text);
						repeat = false;
						switch (operation)
						{
							case "+": 
								result = String(operand1 + operand2);
								break;
							case "-": 
								result = String(operand1 - operand2);
								break;
							case "*": 
								result = String(operand1 * operand2);
								break;
							case "/":
								
								if (String(operand2) == "0")
								{
									gui.calcTextField.text = 'нельзя ';
									result = null;
									operand1 = NaN;
									operand2 = NaN;
									operand2repeat = NaN;
									this.operation = "";
									dontPrintResult = true;
									fistPress = true;
								}
								
								if (!dontPrintResult)
								{
									result = String(operand1 / operand2);
								}
								break;
						}
						
						if (!dontPrintResult)
						{
							operand1 = Number(result);
						}
						operand2 = NaN;
						
						if (!dontPrintResult)
						{
							gui.calcTextField.text = result;
						}
						dontPrintResult = false;
						
						clearDisplay = true;
					}
				}
			}
		}
		
		public function StarlingRoot()
		{
			gui = new GUI();
			addChild(gui);
			gui.addEventListener(GUI.BUTTON_PRESS, onButtonPress);
		}
		
		private function onButtonPress(e:Event):void
		{
			if (gui.value == "1" || gui.value == "2" || gui.value == "3" || gui.value == "4" || gui.value == "5" || gui.value == "6" || gui.value == "7" || gui.value == "8" || gui.value == "9" || gui.value == "0")
			{
				waitInput = false;
				fistPress = false;
				
				if (clearDisplay)
				{
					gui.calcTextField.text = "";
					clearDisplay = false;
				}
				
				if (gui.calcTextField.text.length < 8)
				{
					gui.calcTextField.text += gui.value;
				}
				
			}
			
			switch (gui.value)
			{
				case "+": 
					if (!fistPress)
					{
						if (!repeat)
						{
							executeCalculation(operation); // завершаем предыдущюю операцию
						}
						repeat = false;
						executeCalculation("+");
					}
					break;
				case "-": 
					if (!fistPress)
					{
						if (!repeat)
						{
							executeCalculation(operation); // завершаем предыдущюю операцию
						}
						repeat = false;
						executeCalculation("-");
					}
					break;
				case "*": 
					if (!fistPress)
					{
						if (!repeat)
						{
							executeCalculation(operation); // завершаем предыдущюю операцию
						}
						repeat = false;
						executeCalculation("*");
					}
					break;
				case "/": 
					if (!fistPress)
					{
						if (!repeat)
						{
							executeCalculation(operation); // завершаем предыдущюю операцию
						}
						repeat = false;
						executeCalculation("/");
					}
					break;
				case "=": 
					if (operation != "")
					{
						executeCalculation(operation);
						repeat = true;
						waitInput = false;
					}
					break;
			}
		}
	}
}