package 
{
    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
	import flash.events.Event;
	import starling.core.Starling;
 
    [SWF(width="400", height="400", frameRate="60", backgroundColor="#ffffff")]
    public class Main extends Sprite
    {
        private var mStarling:Starling;
 
        public function Main()
        {            
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;
 
            mStarling = new Starling(StarlingRoot, stage);
            mStarling.start();
        }
    }
}