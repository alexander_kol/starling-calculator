package
{
	import feathers.controls.Button;
	import feathers.controls.text.BitmapFontTextRenderer;
	import flash.utils.Dictionary;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.utils.Color;
	import starling.utils.deg2rad;
	import starling.events.Event
	import starling.utils.HAlign;
	
	/**
	 * ...
	 * @author Alexander.kolobov
	 */
	
	public class GUI extends Sprite
	{
		public static const BUTTON_PRESS:String = "buttonPress";
		private var _value:String;
		private var dict:Dictionary;
		
		[Embed(source="../assets/atlas.xml",mimeType="application/octet-stream")]
		public static const AtlasXML:Class;
		
		[Embed(source="../assets/atlas.png")]
		public static const AtlasTexture:Class;
		
		[Embed(source="../assets/font.fnt",mimeType="application/octet-stream")]
		public static const FontXml:Class;
		
		[Embed(source="../assets/font.png")]
		public static const FontTexture:Class;
		
		public var calcTextField:TextField;
		
		public function GUI()
		{
			var fontTexture:Texture = Texture.fromBitmap(new FontTexture());
			var fontXml:XML = XML(new FontXml());
			TextField.registerBitmapFont(new BitmapFont(fontTexture, fontXml));
			
			var texture:Texture = Texture.fromBitmap(new AtlasTexture());
			var xml:XML = XML(new AtlasXML());
			var atlas:TextureAtlas = new TextureAtlas(texture, xml);
			
			var calcBodyTexture:Texture = atlas.getTexture("calc_bg");
			var calcBodyImage:Image = new Image(calcBodyTexture);
			addChild(calcBodyImage);
			calcBodyImage.x = 72;
			calcBodyImage.y = 72;
			
			dict = new Dictionary();
			
			var mapperUpTexture:Array = ["divide_btn", "minus_btn", "multi_btn", "plus_btn", "1_btn", "2_btn", "3_btn", "0_btn", "4_btn", "5_btn", "6_btn", "result_btn", "7_btn", "8_btn", "9_btn"];
			var mapperPressTexture:Array = ["divide_btn_press", "minus_btn_press", "multi_btn_press", "plus_btn_press", "1_btn_press", "2_btn_press", "3_btn_press", "0_btn_press", "4_btn_press", "5_btn_press", "6_btn_press", "result_btn_press", "7_btn_press", "8_btn_press", "9_btn_press"];
			var buttonsCoordinates:Array = [[120, 150], [163, 150], [207, 150], [250, 150], [115, 180], [163, 180], [208, 180], [253, 180], [112, 215], [162, 215], [209, 215], [255, 215], [110, 250], [162, 250], [210, 250]];
			var mapperOperation:Array = ['/', '-', '*', '+', '1', '2', '3', '0', '4', '5', '6', '=', '7', '8', '9'];
			
			for (var i:int = 0; i < 15; i++)
			{
				var tempBtn:Button = new Button();
				var defaultTexture:Texture = atlas.getTexture(mapperUpTexture[i]);
				var downTexture:Texture = atlas.getTexture(mapperPressTexture[i]);
				tempBtn.defaultSkin = new Image(defaultTexture);
				tempBtn.downSkin = new Image(downTexture);
				addChild(tempBtn);
				tempBtn.x = buttonsCoordinates[i][0];
				tempBtn.y = buttonsCoordinates[i][1];
				tempBtn.addEventListener(Event.TRIGGERED, onTrigged);
				dict[tempBtn] = mapperOperation[i];
			}
			calcTextField = new TextField(150, 60, "", "font", 30);
			calcTextField.x = 121;
			calcTextField.y = 96;
			calcTextField.hAlign = HAlign.RIGHT;
			addChild(calcTextField);
		
			//var textRenderer:BitmapFontTextRenderer = new BitmapFontTextRenderer();
			//textRenderer.text = "123";
			//addChild(textRenderer);
		
		}
		
		private function onTrigged(e:Event):void
		{
			_value = dict[e.currentTarget];
			dispatchEvent(new Event(BUTTON_PRESS));
		}
		
		public function get value():String
		{
			return _value;
		}
	}
}